import bs4
import shutil
import requests

_MIME_EXT = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/gif': 'gif',
}

def guess_extension(mime):
    '''
    Not using mimetypes to control extension.
    '''

    if mime in _MIME_EXT:
        return _MIME_EXT[mime]
    else:
        print('[!] Invalid mime= %s assuming .png'%(mime))
        return 'png'

def pre_rewrite_rule(s: str):
    return s

def pos_rewrite_rule(s: str, fname: str):
    return '/static/%s'%(fname)

def download_images(content: bs4.Tag,
                    pre_rewrite_rule: callable = pre_rewrite_rule,
                    pos_rewrite_rule: callable = pos_rewrite_rule):
    imgs = content.find_all('img')
    idx = 0
    for img in imgs:
        if 'src' in img.attrs:
            prog = '(%02d/%02d)'%(idx, len(imgs))
            src = pre_rewrite_rule(img['src'])
            print('[-] %s Fetching image meta img= %s'%(prog, src))
            resp = requests.head(src)
            mime = resp.headers['Content-Type']
            ext = guess_extension(mime)
            print('[+] %s Image mime= %s ext= %s'%(prog, mime, ext))

            print('[-] %s Downloading img= %s'%(prog, src))
            resp = requests.get(src, stream=True)
            fname = 'output/img%02d.%s'%(idx, ext)
            with open(fname, 'wb') as out:
                shutil.copyfileobj(resp.raw, out)
            print('[+] %s Finished downloading'%(prog))
            nsrc = pos_rewrite_rule(img['src'], fname)
            img['src'] = nsrc
            print('[+] %s Finished rewritting to %s'%(prog, nsrc))
            del resp
            idx += 1

def convert_iframes(soup: bs4.BeautifulSoup, content: bs4.Tag):
    iframes = content.find_all('iframe')
    for iframe in iframes:
        img = soup.new_tag('img')
        img['src'] = iframe['src']

        caption = soup.new_tag('figcaption')
        caption.string = '--converted-iframe--'

        ntag = soup.new_tag('figure')
        ntag.append(img)
        ntag.append(caption)

        iframe.insert_after(ntag)
        print('[+] Converted iframe to image %s'%(iframe['src']))

