import subprocess
import tempfile
import sys

def convert_markdown(html: str, output: str):
    htmlf = tempfile.NamedTemporaryFile(mode='w',
                                        suffix='.html',
                                        prefix='magpie-',
                                        delete=False)
    htmlf.write(html)
    htmlf.close()


    p = subprocess.Popen(['pandoc', '--from',
                            'html',
                            '--to',
                            'gfm-raw_html-native_divs-native_spans', '-o', output,
                            htmlf.name], stdout=sys.stdout,
                            stderr=sys.stderr)
    p.wait()
    return p.returncode == 0


