import bs4


def extract_content(soup: bs4.BeautifulSoup, css_selector: str):
    article = soup.select(css_selector)
    if len(article) > 0:
        return article[0]
    else:
        return None




