import sys
import os
import argparse
import bs4
import requests

from magpie.boilerplate import extract_content
from magpie.images import download_images, convert_iframes
from magpie.pandoc import convert_markdown

def main():
    parser = argparse.ArgumentParser(description='Extract articles in md format')
    parser.add_argument('-o', '--output', default='out.md',
            help='Output filename with md content')
    parser.add_argument('-s', '--selector', default='body',
            help='CSS Selector where article is')
    parser.add_argument('url', type=str, help='URL to be extracted')
    args = parser.parse_args()

    con = requests.get(args.url)
    soup = bs4.BeautifulSoup(con.content, 'lxml')
    ret = extract_content(soup, args.selector)

    def rewrite(s):
        return s.replace('/max/60/', '/max/700/')

    #download_images(ret, rewrite)
    convert_iframes(soup, ret)
    convert_markdown(str(soup), args.output)

if __name__ == '__main__':
    main()
